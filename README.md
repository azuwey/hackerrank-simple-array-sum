# Simple Array Sum

## Problem

Given an array of `N` integers, can you find the sum of its elements?

### Input Format

The first line contains an integer, `N`, denoting the size of the array. The second line contains `N` space-separated integers representing the array's elements.

### Output Format

Print the sum of the array's elements as a single integer.

#### Sample Input

```s
6
1 2 3 4 10 11
```

#### Sample Output

```s
31
```

#### Explanation

We print the sum of the array's elements, which is: 1 + 2 + 3 + 4 + 10 + 11 = 31.

## Source

[HackerRank - Simple Array Sum](https://www.hackerrank.com/challenges/simple-array-sum/problem)